# SLOCK - Simple Lock.
Simple X display locker. This is the simplest X screen locker I am aware of. It is stable and quite 
a lot of people in the suckless community are using it every day. It really amazes me, How Simple and Yet Secure it is. 

## Prerequisites. 
- Xlib Header Files. 
- Dwm. 
If you want your system to run slock after a certain period of inactivity: 
- Xautolock. Append the following to your ~/.xinitrc:
```bash
echo "xautolock -time 3 -locker slock" >> ~/.xintrc
```

## Download. 
Clone the Repository:
```bash
git clone https://gitlab.com/crimsonmyth/slock.git
```

## Install.

```bash
cd slock/
sudo make clean install
```
You have now installed slock, if you want to bind it to keys then I would recommend using a hotkey daemon or bind it 
through your window manager itself. 

